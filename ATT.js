const express = require("express");
const request = require( 'request-promise-native');
const underscore = require('underscore');
const HEADERS = {
    'User-Agent': 'att-test',
    'Authorization': 'token 5cad395c93de45d6721b44a1d5929ce248d17c5b'
};
const app = express();
app.get('/issues', async (req, res, next) => {
    const repos = await getRepos();
    const issues = await partitionRequests(repos, getIssues, true);
    const comments = await partitionRequests(issues, getComments, false);
    const issuesWithComments = generateIssuesWithComments(issues, comments);
    res.json(issuesWithComments);
});
async function getRepos() {
    return await request({
        uri: 'https://api.github.com/orgs/att/repos',
        headers: HEADERS,
        json:true
    });
}
async function getIssues(repo) {
    return await request({
        uri: `https://api.github.com/repos/att/${repo.name}/issues`,
        headers: HEADERS,
        json: true
    });
}
async function getComments(issue) {
    return await request({
        uri: issue.comments_url,
        headers: HEADERS,
        json: true
    });
}
// paritition list of requests by RATE_LIMIT
// return result array with combined asyncCall's response
async function partitionRequests(arr, asyncCall, flatten) {
    let result = [];
    const RATE_LIMIT = 60;
    let i = arr.length > RATE_LIMIT ? RATE_LIMIT : arr.length;
    let count = 0;
    while(i <= arr.length) {
        const promises = arr.slice(count * RATE_LIMIT, i).map(async (elem) => {
            return await asyncCall(elem);
        });
        let resultPartial = await Promise.all(promises);
        result = result.concat(resultPartial);
        if(i === arr.length) {
            break;
        }
        i = (i + RATE_LIMIT > arr.length) ? arr.length : (i + RATE_LIMIT);
        count++;
    }
    return flatten ? underscore.flatten(result, true) : result;
}
// combined issues with comments
function generateIssuesWithComments(issues, comments) {
    return issues.map((issue, index) => {
        return {
            url: issue.url,
            title: issue.title,
            body: issue.body,
            commentsCount: issue.comments,
            comments: comments[index]
        }
    });
}
app.listen(process.env.PORT || 3000);
console.log("att server start");